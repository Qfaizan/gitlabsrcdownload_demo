/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./src/**/*.{js,jsx,ts,tsx}", "./src/**/**/*.{js,jsx,ts,tsx}"],
    theme: {
        extend: {
            boxShadow: {
                btn: "0px 1px 2px rgba(153, 153, 153, 0.15), 0px 1px 3px 1px rgba(0, 0, 0, 0.3) !important",
                "btn-error":
                    "0px 1px 2px rgba(153, 153, 153, 0.15), 0px 1px 3px 1px rgb(227 19 19 / 48%) !important",
                "3xl": "0 35px 60px -15px rgba(0, 0, 0, 0.3)",
                "2xl": "0 25px 50px -12px rgb(0 0 0 / 0.25)"
            },
            backgroundColor: {
                transparent: "rgb(0 0 0 / 59%)",
                "fill-active": "#FF802E",
                "fill-hover": "#FAC21A",
                fill: "#ff8d43",
                "fill-focus": "#fca523",
                disabled: "rgba(16, 16, 16, 0.12)"
            },
            colors: {
                disabled: "rgba(16, 16, 16, 1) !important"
            },
            width: {
                "25p": "25%",
                "24p": "24%",
                "23p": "23%",
                "22p": "22%",
                "45p": "45%",
                "46p": "46%",
                "47p": "47%",
                "48p": "48%",
                "49p": "49%",
                "50p": "50%",
                500: "500px"
            },
            minHeight: {
                normal: "80px",
                sm: "60px"
            }
        }
    },
    plugins: []
};
