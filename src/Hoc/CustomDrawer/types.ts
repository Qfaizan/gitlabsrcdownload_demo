export interface CustomDrawerProps {
    children: any;
    type: string;
    handleDrawerOnClose?: () => void;
    title?: String;
}
