export interface CardComponentProps {
    cardTitle?: string;
    children: React.ReactNode;
    paperHeight?: any;
    buttonText1?: string;
    buttonText2?: string;
    handleBack?: () => void;
    handleNext?: () => void;
    buttonType?: any;
}
