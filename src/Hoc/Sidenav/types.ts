export type SideNavbarProps = {
    state: any;
    setState?: any;
    toggleDrawer: any;
    listParentText?: any;
    selected?: any;
    Paths?: any;
    Menus?: [] | any;
    logo?: any;
    onChange: (childMenuText: string) => void;
};
