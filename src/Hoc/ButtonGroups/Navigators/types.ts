export type NavigatorsProps = {
    handleBack?: () => any;
    handleNext?: () => any;
    submit?: boolean;
    disableNext?: boolean;
    buttonText1?: any;
    buttonText2?: any;
    buttonType?: any;
};
