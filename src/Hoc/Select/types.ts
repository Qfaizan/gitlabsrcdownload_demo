import { SelectOptions } from "../../types/InputProps";

export interface SelectComponentProps {
    id?: string;
    value: SelectOptions | string | null;
    name?: string;
    label?: string;
    onChange: (value: SelectOptions | null | string) => void;
    required?: boolean | null;
    error?: boolean;
    toolTip?: string;
    placeHolder?: string;
    helperText?: string;
    defaultValue?: SelectOptions | string | null;
    disabled?: boolean;
    list: SelectOptions[] | string[] | null;
    onBlur?: any;
    onInputChange?: (value?: any) => void;
    errorText?: any;
    allowAddList?: boolean;
    addListToDatabse?: boolean;
    noOptionsText?: string;
}
