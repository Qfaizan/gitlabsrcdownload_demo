import React from "react";

export interface ModelProps {
    children?: React.ReactNode;
    open?: boolean;
    onClose?: () => void;
    title?: string;
    disableEscapeKeyDown?: boolean;
    xl?: number;
    sm?: number;
    xs?: number;
    lg?: number;
    closeIcon?: boolean;
}
