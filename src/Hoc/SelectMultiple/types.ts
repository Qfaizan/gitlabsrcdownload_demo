export interface SelectMultipleProps {
    id: string;
    value: string[];
    name: string;
    label: string;
    onChange: (newValue: string[]) => void;
    required?: boolean;
    error: boolean;
    toolTip?: string;
    helperText?: string;
    defaultValue?: any;
    list: (string | { value: string; label: string })[];
    errorText?: string;
}
