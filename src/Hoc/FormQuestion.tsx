import React from "react";

import { Grid, Paper, TextField, Typography } from "@mui/material";

interface FormQuestionInputProps {
    label: string;
    inputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
    error?: boolean;
    helperText?: string;
    value: string;
}

const FormQuestionInput: React.FC<FormQuestionInputProps> = ({ label, inputChange, error = false, helperText = "", value }) => {
    return (
        <Paper
            style={{
                padding: "16px",
                marginBottom: "16px"
            }}
            elevation={3}
        >
            <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                    <Typography variant="subtitle1" style={{ fontWeight: "bold" }}>
                        {label}:
                    </Typography>
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        variant="outlined"
                        value={value}
                        fullWidth
                        placeholder={`Enter ${label.toLowerCase()}`}
                        onChange={inputChange}
                        error={error}
                        helperText={helperText}
                    />
                </Grid>
            </Grid>
        </Paper>
    );
};

export default FormQuestionInput;
