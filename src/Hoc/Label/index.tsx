import React from "react";

import InputLabel from "@mui/material/InputLabel";

export const Label = ({ name }: any) => {
    return <InputLabel id="demo-select-small-label">{name}</InputLabel>;
};
