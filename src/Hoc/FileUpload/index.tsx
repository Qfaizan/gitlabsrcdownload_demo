import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

import { Button } from "@mui/material";

import { en, messages } from "../../dictionary/en";
import RoutesEnum from "../../enums/Routes.enum";
import { updateProjectFromJson, updateTechnologyFromJson } from "../../Reduxt/action";
import { buttonStyle } from "../../Theme/Theme";

function JsonFileUpload() {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [fileContent, setFileContent] = useState<string | null>(null);
    const [editButton, setEditButton] = useState(false);

    const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files?.[0];

        if (file) {
            const reader = new FileReader();
            reader.onload = (e) => {
                const fileContentValue = e.target?.result as string;
                setFileContent(fileContentValue);
            };

            reader.readAsText(file);
            setEditButton(true);
        }
    };

    if (fileContent) {
        try {
            const parsedData = JSON.parse(fileContent);
            dispatch(
                updateProjectFromJson({
                    projectName: parsedData.projectName,
                    tagName: parsedData.tagName,
                    base64Logo: parsedData.base64Logo,
                    faviIconBase64Image: parsedData.faviIconBase64Image,
                    themeColor: parsedData.themeColor,
                    appFont: parsedData.appFont
                })
            );
            dispatch(
                updateTechnologyFromJson({
                    frontend: parsedData.frontend,
                    api: parsedData.api,
                    database: parsedData.database,
                    host: parsedData.host,
                    Component: parsedData.Component
                })
            );
        } catch (error) {
            console.error(messages.jsonparseerror, error);
        }
    }

    const handleClick = () => {
        navigate(RoutesEnum.editjson);
    };

    return (
        <div>
            <input type="file" accept=".json" onChange={handleFileChange} />
            <div>
                <h3>File Content:</h3>
                <pre>{fileContent}</pre>
            </div>
            <div>
                {editButton && (
                    <Button variant="contained" color="primary" style={buttonStyle} onClick={handleClick}>
                        {en.editproject}
                    </Button>
                )}
            </div>
        </div>
    );
}

export default JsonFileUpload;
