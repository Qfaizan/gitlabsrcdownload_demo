import React, { useState } from "react";

import CloudUploadIcon from "@mui/icons-material/CloudUpload";
import Button from "@mui/material/Button";

interface ImageProps {
    image: File | null;
    base64Image: string;
    handleImageChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

function WithImageUpload<P>(BaseComponent: React.ComponentType<P & { imageProps: ImageProps }>) {
    return function ImageUploadHOC(props: P) {
        const [image, setImage] = useState<File | null>(null);
        const [base64Image, setBase64Image] = useState<string>("");

        const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
            const selectedImage = e.target.files?.[0];

            if (selectedImage) {
                const reader = new FileReader();
                reader.onload = (event: any) => {
                    setBase64Image(event?.target?.result as string);
                };
                reader.readAsDataURL(selectedImage);
                setImage(selectedImage);
            }
        };

        const imageProps: ImageProps = {
            image,
            base64Image,
            handleImageChange
        };

        return <BaseComponent {...props} imageProps={imageProps} />;
    };
}

interface YourComponentProps {
    imageProps: ImageProps;
}

function YourComponent({ imageProps }: YourComponentProps) {
    const { base64Image, handleImageChange } = imageProps;

    return (
        <div>
            <input type="file" accept="image/*" onChange={handleImageChange} style={{ display: "none" }} id="image-upload-input" />
            <label htmlFor="image-upload-input">
                <Button variant="contained" component="span" startIcon={<CloudUploadIcon />}>
                    Upload Logo
                </Button>
            </label>
            {base64Image && (
                <div>
                    <h2>Uploaded Image</h2>
                    <img src={base64Image} alt="Uploaded" style={{ maxWidth: "300px", maxHeight: "300px" }} />
                    <p>Base64 Encoded Image:</p>
                    <textarea rows={5} cols={50} readOnly value={base64Image} />
                </div>
            )}
        </div>
    );
}

export default WithImageUpload;
