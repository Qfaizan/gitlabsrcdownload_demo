export type TableProps = {
    tableHead: string[];
    tableData: [] | any;
    editButton: React.ReactNode;
    deleteButton: React.ReactNode;
    downloadButton: React.ReactNode;
    handleEditData: (e: any) => void;
    handleDeleteData: (e: any) => void;
    handleDownload: (e: any, filename: any) => void;
};
