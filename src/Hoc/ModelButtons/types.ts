export interface ModalButtonProps {
    okText: string;
    disableOkBtn?: boolean;
    disableDelete?: boolean;
    disableCancel?: boolean;
    cancelText?: string;
    deleteText?: string;
    onDelete?: () => void;
    onCancel?: () => void;
    deleteAlertText?: string;
    cancelAlertText?: string;
    loading?: boolean;
    onOk?: () => void;
}
