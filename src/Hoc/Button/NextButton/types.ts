export type NextButtonProps = {
    handleClick?: () => void;
    submit?: any;
    disableNext?: any;
    buttonText?: any;
    buttonType?: any;
};
