import React from "react";

import { Button } from "@mui/material";

const SaveButton = ({ handleSubmit, text }: any) => {
    return <Button onClick={handleSubmit}>{text || "Save"}</Button>;
};
export default SaveButton;
