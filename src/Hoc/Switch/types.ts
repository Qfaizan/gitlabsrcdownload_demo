export type switchProps = {
    label?: any;
    handleSwitch?: any;
    checked?: any;
    labelPlacement?: "start" | "end" | "top" | "bottom";
};
