import { ReactNode } from "react";

export interface SelectBoxPropsType {
    option: {
        value: string;
        label: string;
        title?: string;
        icon?: ReactNode;
        discription?: string;
    };
    openDrawer?: any;
    category?: string;
    setSelectedOptions: (value: any, category?: any) => void;
    selectedOptions?: any;
    totalOption?: any;
}
