import axios from "axios";

const baseURL = process.env.REACT_APP_API_BASEURL;

const useAxios = axios.create({ baseURL });

export default useAxios;
