import React from "react";
import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";

import Alert from "./Hoc/AlertMessage";
import { initalState } from "./Redux/store";
import App from "./App";

import "./index.css";

const container = document.getElementById("root");
const root = createRoot(container!);

root.render(
    <Provider store={initalState}>
        <React.StrictMode>
            <Alert />
            <App />
        </React.StrictMode>
    </Provider>
);
