import APiEndPoints from "../enums/ApiEndPoints";

import axios from "./Configuration/apiConfiguration";

const getHostByLayer = (data: any) =>
    new Promise((resolve, reject) => {
        axios
            .get(`${APiEndPoints.Host}/layertype/${data} `, {})
            .then((response) => {
                resolve({ success: true, data: response.data });
            })
            .catch((error) => {
                reject(error);
            });
    });
const updateHost = (data: any) =>
    new Promise((resolve, reject) => {
        axios
            .put(APiEndPoints.Host, { data })
            .then((response) => {
                resolve({ success: true, data: response.data });
            })
            .catch((error) => {
                reject(error);
            });
    });
const createHostApi = (data: any) =>
    new Promise((resolve, reject) => {
        axios
            .post(APiEndPoints.Host, { data })
            .then((response) => {
                resolve({ success: true, data: response.data });
            })
            .catch((error) => {
                reject(error);
            });
    });
export default { createHostApi, getHostByLayer, updateHost };
