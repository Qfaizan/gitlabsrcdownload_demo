import APiEndPoints from "../enums/ApiEndPoints";

import axios from "./Configuration/apiConfiguration";

const getPagesByID = (id: any) =>
    new Promise((resolve, reject) => {
        axios
            .get(`${APiEndPoints.Page}/${id}`, {})
            .then((response) => {
                resolve({ success: true, data: response.data });
            })
            .catch((error) => {
                reject(error);
            });
    });

export default {
    getPagesByID
};
