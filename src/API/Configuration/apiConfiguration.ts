import axios from "axios";

const token = localStorage.getItem("token") || "";
export default axios.create({
    baseURL: process.env.REACT_APP_API_BASEURL || "",
    // baseURL: "http://localhost:8080" || process.env.REACT_APP_BASE_URL || "",
    timeout: 50000000,
    headers: { token: token ? JSON.parse(token!) : "" }
});
