import { toastConfigType } from "../Component/ManageProject/BrandingAndLogo/types";

export interface loaderState {
    loading: boolean;
}
export interface buttonsOptionsType {
    backgroundColor: string;
    fontColor: string;
    variant: "outlined" | "contained";
}
export interface buttonsType {
    primary: buttonsOptionsType;
    secondary: buttonsOptionsType;
}
export interface projectDetailsState {
    projectName: string;
    tagName: string;
    base64Logo: string;
    faviIconBase64Image: string;
    themeColor: string;
    appFont: string;
    buttons?: buttonsType;
    toastConfig: toastConfigType;
    Menus?: MenusType[];
    Navbar?: { key: string; value: string };
}
export interface MySqlDBType {
    databaseName: "mysql";
    hostName: string;
    userName: string;
    password: string;
}
export interface MsSqlDBType {
    databaseName: "mssql";
    servername: string;
    login: string;
    password: string;
}
export interface MongoDBType {
    databaseName: "mongodb";
    connectionString: string;
}
export interface technologyState {
    frontend: string;
    api: string;
    database: MySqlDBType | MsSqlDBType | MongoDBType;
}
export interface MenusType {
    addToMenu: boolean;
    childMenuText: string;
    path: string;
}
export interface EmailLoginType {
    authenticationType: "emailLogin";
    email?: string;
    password?: string;
}
export interface GoogleLoginType {
    authenticationType: "google";
    clientId: string;
}
export interface Auth0LoginType {
    authenticationType: "auth0";
    clientId: string;
    domain: string;
}
export interface FacebookLoginType {
    authenticationType: "facebook";
    appId: string;
    appSecret: string;
    reDirectUris: string;
}
export interface VimeoMediaType {
    mediaType: "vimeo" | any;
    clientId?: string;
    accessToken?: string;
}
export interface YoutubeMediaType {
    mediaType: "youtube";
    googleapiKey?: string;
    oauthClientId?: string;
}
export interface FacebookMediaType {
    mediaType: "facebookmedia";
    appId?: string;
    appSecret?: string;
}
type paymenttypeType = "razPay" | "payPal" | "stripe" | "";
export interface RazorPayCredentialsType {
    paymentType: paymenttypeType;
    apiKey?: string;
    keySecret?: string;
}
export interface PaypalCredentialsType {
    paymentType: paymenttypeType;
    clientID?: string;
    secretKey?: string;
}
export interface StripeCredentialsType {
    paymentType: paymenttypeType;
    publicKey?: string;
    secretKey?: string;
}
export interface FtpCredentialsType {
    fileUploadType: string;
    serverAddress?: string;
    clientSecret?: string;
}
export interface StorageCredentialsType {
    fileUploadType: string;
    clientID?: string;
    clientSecret?: string;
}

export type AuthenticationType = EmailLoginType | Auth0LoginType | GoogleLoginType | FacebookLoginType;
export type MediaType = VimeoMediaType | YoutubeMediaType | FacebookMediaType | any;
export type PaymentType = RazorPayCredentialsType | PaypalCredentialsType | StripeCredentialsType;
export type FileUploadType = FtpCredentialsType | StorageCredentialsType;
export interface componentsType {
    Authentication: AuthenticationType[];
    "File Upload": FileUploadType[];
    Media: MediaType[];
    Payments: PaymentType[];
    Chat: boolean;
}
export interface newProjectState {
    projectDetails: projectDetailsState;
    technology: technologyState;
    component: componentsType;
    _id?: any;
}
export interface WebAppServiceState {
    webAppName: string;
    webAppRegion: string;
    runTimeStack: string;
    publish: string;
    operatingSystem: string;
    gitBranchName: string;
    gitUrl: string;
    appServicePlanName: string;
    appServiceRegion: string;
    pricingTier: string;
}
export interface labelValueState {
    label: string;
    value: string;
}
export interface DiskStorageState {
    disk: string;
    diskType: labelValueState;
    diskSize: labelValueState;
}
export interface AuthTypeState {
    AuthorizationType: string;
    userNameSSH?: string;
    keyPairName?: string;
    userName?: string;
    password?: string;
    confirmPassword?: string;
}
export interface VirtualMachineState {
    hostType: string;
    VmName: string;
    region: string;
    image: string;
    size: string;
    authType: AuthTypeState;
    publicInboundPorts: [];
    diskStorageDetails: DiskStorageState;
    virtualNetwork: string;
}
export interface HostUiLayerState {
    virtualMachine: VirtualMachineState;
    webAppServices: WebAppServiceState;
}
export interface newPageState {
    pageType: string;
    pageName: string;
    defaultCheckbox?: boolean;
    pageDetails: Object[];
}

export interface alertMessageProps {
    open?: boolean;
    message?: string | any[];
    type?: "error" | "info" | "success" | "warning";
    duration?: number;
    onAction?: (data?: any) => void;
    actionText?: string;
    closeIcon?: boolean;
    closableOnBackDropClick?: boolean;
    position?: string;
    transitionState?: {
        slide?: "up" | "down" | "right" | "left";
        grow?: boolean;
        zoom?: boolean;
    };
}
export interface drawerType {
    open: Boolean;
    type: String;
    title: String;
}
export interface shhKeyType {
    authorizationType: string;
    userNameSSH?: string;
    keyPairName?: string;
}
export interface passwordType {
    authorizationType: string;
    userName?: string;
    password?: string;
}
export interface InitialState {
    projectData: [];
    loaderState: loaderState;
    userData: any;
    newProject: newProjectState;
    hostLayerType: any;
    hostUi: HostUiLayerState | any;
    hostApiLayer: HostUiLayerState | any;
    hostDbLayer?: HostUiLayerState | any;
    hostServiceLayer?: any;
    pageData: [];
    newPages?: newPageState;
    alertMessage?: any;
    drawerState?: drawerType;
}
