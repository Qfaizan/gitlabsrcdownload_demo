import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

import Grid from "@mui/material/Grid";
import Box from "@mui/system/Box";

import RoutesEnum from "../../../enums/Routes.enum";
import HoC from "../../../Hoc";
import { createHostConfig } from "../../../inputconfig";
import { useAppDispatch, useAppSelector } from "../../../Redux/Hooks";
import { handleHostUi } from "../../../Redux/Reducers";

import { ApiAppTypeOptions } from "./Utilities";

const ApiLayer = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [selectedOptions, setSelectedOptions] = useState<string>("");
    const { hostApiLayer } = useAppSelector((state) => state);
    console.log(hostApiLayer);
    const handleOptionsSubmit = () => {
        dispatch(handleHostUi({ hostType: selectedOptions }));
        switch (selectedOptions) {
            case createHostConfig?.appValue?.value1:
                break;
            case createHostConfig?.appValue?.value2:
                navigate(RoutesEnum.webAppDetails);
                break;
            case createHostConfig?.appValue?.value3:
                navigate(RoutesEnum.functionAppDetails);
                break;
            case createHostConfig?.appValue?.value4:
                navigate(RoutesEnum.dbSql);
                break;
            default:
                break;
        }
    };
    return (
        <HoC.CardComponent cardTitle="Host Type">
            <Box p={5} px={10} className="cardComponentInsideWrapper">
                <Box className="manageProjectWrapper">
                    <Grid container spacing={2}>
                        {ApiAppTypeOptions.map((option) => (
                            <Grid item xs={12} sm={6}>
                                <HoC.SelectBox option={option} setSelectedOptions={setSelectedOptions} selectedOptions={selectedOptions} />
                            </Grid>
                        ))}
                    </Grid>
                </Box>
                <Box py={2}>
                    <HoC.Navigators handleNext={handleOptionsSubmit} disableNext={false} />
                </Box>
            </Box>
        </HoC.CardComponent>
    );
};

export default ApiLayer;
