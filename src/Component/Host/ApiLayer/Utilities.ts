import ComputerIcon from "@mui/icons-material/Computer";
import FunctionsIcon from "@mui/icons-material/Functions";
import SettingsApplicationsIcon from "@mui/icons-material/SettingsApplications";

import { createHostConfig } from "../../../inputconfig";

import { Options } from "./types";

export const ApiAppTypeOptions: Options[] = [
    {
        value: createHostConfig?.appValue?.value1,
        label: createHostConfig?.appType?.type1,
        icon: ComputerIcon
    },
    {
        value: createHostConfig?.appValue?.value2,
        label: createHostConfig?.appType?.type2,
        icon: SettingsApplicationsIcon
    },
    {
        value: createHostConfig?.appValue?.value3,
        label: createHostConfig?.appType?.type3,
        icon: FunctionsIcon
    }
];
