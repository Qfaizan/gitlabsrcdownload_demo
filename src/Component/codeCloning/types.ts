export type DownloadCodeProps = {
    jsonData?: {
        pageName?: string;
        pageType?: string;
    };
};
