export interface JsonData {
    pageDetails: {
        columnName: string;
        columnValue: string;
        columnType?: {
            label: string;
        };
    }[];
    pageType?: string;
    pageName?: string;
    projectDetails?: {
        appFont: string;
        base64Logo: string;
        faviIconBase64Image: string;
        projectName: string;
        tagName: string;
        themeColor: string;
    };
}
