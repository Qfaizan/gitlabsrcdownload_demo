import React, { useEffect, useState } from "react";

import { Edit } from "@mui/icons-material";
import { Box, Button, Divider, Drawer, Grid, Typography } from "@mui/material";

import { en } from "../../dictionary/en";
import { buttonStyle } from "../../Theme/Theme";
import EditDataFormPage from "../Form/Editpage/EditDataForm";
import EditListFormPage from "../Form/Editpage/EditListForm";
import EditReportFormPage from "../Form/Editpage/EditReportForm";

const EditPage = ({ page }: any) => {
    const [pageData, setPageData] = useState<any>(page[0]);
    console.log("setPageData: ", setPageData);
    const [drawerOpen, setDrawerOpen] = useState(false);
    const [dataPage, setDataPage] = useState(false);
    const [listPage, setListPage] = useState(false);
    const [reportPage, setReportPage] = useState(false);
    const handleDrawerOpen = async () => {
        setDrawerOpen(true);
    };
    useEffect(() => {
        if (pageData?.pageType) {
            if (pageData?.pageType.replace(/\s+/g, "").toLowerCase() === en.lowdataform) {
                setDataPage(true);
            } else if (pageData?.pageType.replace(/\s+/g, "").toLowerCase() === en.lowlistform) {
                setListPage(true);
            } else if (pageData?.pageType.replace(/\s+/g, "").toLowerCase() === en.lowreportform) {
                setReportPage(true);
            }
        }
    }, [pageData]);
    console.log({ pageData });
    const handleDrawerClose = () => {
        setDrawerOpen(false);
    };
    return (
        <>
            <Button onClick={handleDrawerOpen} variant="contained" style={buttonStyle}>
                <Edit />
            </Button>
            <Drawer variant="temporary" anchor="right" open={drawerOpen} onClose={handleDrawerClose}>
                <Box sx={{ maxWidth: "660px", width: "100%", display: "flex", justifyContent: "center" }}>
                    <Grid container p={2} rowGap={2}>
                        <Grid item xs={12} py={2}>
                            <Box>
                                <Typography>Edit Page</Typography>
                            </Box>
                            <Divider variant="middle" />
                        </Grid>
                        <Grid item xs={12} py={2}>
                            {dataPage ? (
                                <EditDataFormPage
                                    jsonDatavalue={pageData}
                                    onPropsHandleChange={() => {
                                        setDataPage(false);
                                    }}
                                />
                            ) : listPage ? (
                                <EditListFormPage
                                    jsonDatavalue={pageData}
                                    onPropsHandleChange={() => {
                                        setListPage(false);
                                    }}
                                />
                            ) : reportPage ? (
                                <EditReportFormPage
                                    jsonDatavalue={pageData}
                                    onPropsHandleChange={() => {
                                        setReportPage(false);
                                    }}
                                />
                            ) : null}
                        </Grid>
                    </Grid>
                </Box>
            </Drawer>
        </>
    );
};

export default EditPage;
