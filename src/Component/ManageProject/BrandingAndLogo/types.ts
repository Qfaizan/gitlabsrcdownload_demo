export interface buttonsOptionsType {
    color: string;
    backgroundColor: string;
    variant: "contained" | "outlined";
}
export type toastPositionType = "top-right" | "top-center" | "bottom-right" | "bottom-center" | undefined;

export type toastTypeType = "info" | "success" | "warn" | "error";
export type toastThemeType = "light" | "dark" | "colored";
export interface toastConfigType {
    position: toastPositionType;
    type: toastTypeType;
    theme: toastThemeType;
}
export type toastConfigOptionTitleType = "position" | "type" | "theme";
export interface toastConfigOptionOptionsType {
    label: string;
    value: toastPositionType | toastTypeType | toastThemeType;
}

export interface toastConfigOptionType {
    title: toastConfigOptionTitleType;
    options: toastConfigOptionOptionsType[];
}
