export interface optionType {
    label: string;
    value: string;
    icon: any;
}
