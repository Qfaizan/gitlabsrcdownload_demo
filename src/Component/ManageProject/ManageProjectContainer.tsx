import React, { ReactNode } from "react";

import { Container, Grid, Paper, Typography } from "@mui/material";

import Navigators from "../../Hoc/ButtonGroups/Navigators";

interface ManageProjectContainerProps {
    heading: string;
    handleBack: () => void;
    handleNext: () => void;
    disableNext: boolean;
    children: ReactNode;
}
const ManageProjectBox = ({ children, heading, handleBack, handleNext, disableNext }: ManageProjectContainerProps) => {
    return (
        <div
            style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                minHeight: "100vh"
            }}
        >
            <Container maxWidth="md">
                <Paper
                    style={{
                        padding: "16px"
                    }}
                    elevation={3}
                    sx={{ minHeight: "50vh", maxHeight: { xs: "100%", lg: "55vh" }, height: { lg: "55vh" } }}
                >
                    <Grid container direction="column" justifyContent="space-around" alignItems="stretch" sx={{ minHeight: "100%" }}>
                        <Grid item xs={12}>
                            <Typography variant="h5">{heading}</Typography>
                        </Grid>
                        <Grid item xs={12} p={3}>
                            {children}
                        </Grid>
                        <Grid item xs={12}>
                            <Navigators handleBack={handleBack} handleNext={handleNext} disableNext={disableNext} />
                        </Grid>
                    </Grid>
                </Paper>
            </Container>
        </div>
    );
};

export default ManageProjectBox;
