import * as React from "react";
import { Link, useLocation } from "react-router-dom"; // Import Link from React Router

import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import Typography from "@mui/material/Typography";
import { makeStyles } from "@mui/styles";

import api from "../../assets/api.png";
import database from "../../assets/database.png";
import generate from "../../assets/generate.png";
import Logo from "../../assets/Logo.png";
import managePage from "../../assets/managePage.png";
import manageProject from "../../assets/manageProject.png";
import scheduled from "../../assets/scheduled.png";
import services from "../../assets/services.png";
import ui from "../../assets/ui.png";
import RoutesEnum from "../../enums/Routes.enum";
import { useAppDispatch } from "../../Redux/Hooks";
import { handleHost } from "../../Redux/Reducers";

import { TemporaryDrawerProps } from "./types";

const useStyles: any = makeStyles({
    listItem: {
        "&:hover": {
            backgroundColor: "rgba(25, 118, 210, 0.08)" // Change the background color on hover
        }
    }
});

const TemporaryDrawer: React.FC<TemporaryDrawerProps> = ({ state, toggleDrawer }) => {
    const classes = useStyles();
    const location = useLocation();
    const dispatch = useAppDispatch();
    const [selected, setSelected] = React.useState("");

    const path = `/${location.pathname.split("/")[1]}`;
    console.log(path);
    const handleLocation = (hostLayer: any) => {
        setSelected(hostLayer);
        dispatch(handleHost({ layer: hostLayer }));
    };
    React.useEffect(() => {
        if (path !== "/selectapptype") {
            setSelected("");
        }
    }, [path]);

    const list = (anchor: string) => (
        <Box
            role="presentation"
            onClick={toggleDrawer(anchor, false)}
            onKeyDown={toggleDrawer(anchor, false)}
            sx={{ padding: 2, backgroundColor: "#ffffff", height: "100%" }}
        >
            <List>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1, color: "#e40046", display: "flex", alignItems: "center" }}>
                    <img src={Logo} alt="Logo" style={{ width: "100px" }} />
                </Typography>
            </List>
            <Divider />
            <List>
                <ListItem disablePadding>
                    <ListItemText
                        sx={{
                            color: "#948f8f"
                        }}
                        primary="Projects"
                    />
                </ListItem>
                <ListItem disablePadding selected={path === RoutesEnum.manageProject} className={classes.listItem}>
                    <ListItemButton component={Link} to={RoutesEnum.manageProject}>
                        <img src={manageProject} style={{ width: "19px", marginRight: "5px", fontWeight: "bold" }} alt="Projects" />
                        <ListItemText
                            sx={{
                                "&:active": {
                                    color: "#e40046"
                                }
                            }}
                            primary="Manage Project"
                        />
                    </ListItemButton>
                </ListItem>
                <ListItem disablePadding selected={path === RoutesEnum.generateProject} className={classes.listItem}>
                    <ListItemButton component={Link} to={RoutesEnum.generateProject}>
                        <img src={generate} style={{ width: "19px", marginRight: "5px", fontWeight: "bold" }} alt="Manage Project" />
                        <ListItemText
                            sx={{
                                "&:active": {
                                    color: "#e40046"
                                }
                            }}
                            primary="Generate Project"
                        />
                    </ListItemButton>
                </ListItem>
            </List>
            <Divider />
            <List>
                <ListItem disablePadding>
                    <ListItemText
                        sx={{
                            color: "#948f8f"
                        }}
                        primary="Pages"
                    />
                </ListItem>
                <ListItem disablePadding selected={path === RoutesEnum.managePage} className={classes.listItem}>
                    <ListItemButton component={Link} to={RoutesEnum.managePage}>
                        <img src={managePage} style={{ width: "19px", marginRight: "5px", fontWeight: "bold" }} alt="Pages" />
                        <ListItemText
                            sx={{
                                "&:active": {
                                    color: "#e40046"
                                }
                            }}
                            primary="Manage Page"
                        />
                    </ListItemButton>
                </ListItem>
                <ListItem disablePadding selected={path === RoutesEnum.generatePage} className={classes.listItem}>
                    <ListItemButton component={Link} to={RoutesEnum.generatePage}>
                        <img src={generate} style={{ width: "19px", marginRight: "5px", fontWeight: "bold" }} alt="Manage Page" />
                        <ListItemText
                            sx={{
                                "&:active": {
                                    color: "#e40046"
                                }
                            }}
                            primary="Generate Page"
                        />
                    </ListItemButton>
                </ListItem>
            </List>
            <Divider />
            <List>
                <ListItem disablePadding>
                    <ListItemText
                        sx={{
                            color: "#948f8f"
                        }}
                        primary="Host"
                    />
                </ListItem>
                <ListItem disablePadding selected={selected === "uiLayer"} className={classes.listItem}>
                    <ListItemButton component={Link} to={RoutesEnum.selectAppType} onClick={() => handleLocation("uiLayer")}>
                        <img src={ui} style={{ width: "19px", marginRight: "5px", fontWeight: "bold" }} alt="UI Layer" />
                        <ListItemText
                            sx={{
                                "&:active": {
                                    color: "#e40046"
                                }
                            }}
                            primary="UI Layer"
                        />
                    </ListItemButton>
                </ListItem>
                <ListItem disablePadding selected={selected === "apiLayer"} className={classes.listItem}>
                    <ListItemButton component={Link} to={RoutesEnum.selectAppType} onClick={() => handleLocation("apiLayer")}>
                        <img src={api} style={{ width: "19px", marginRight: "5px", fontWeight: "bold" }} alt="API Layer" />
                        <ListItemText
                            sx={{
                                "&:active": {
                                    color: "#e40046"
                                }
                            }}
                            primary="API Layer "
                        />
                    </ListItemButton>
                </ListItem>
                <ListItem disablePadding selected={path === RoutesEnum.servicesLayer} className={classes.listItem}>
                    <ListItemButton component={Link} to={RoutesEnum.servicesLayer} onClick={() => handleLocation("serviceLayer")}>
                        <img src={services} style={{ width: "19px", marginRight: "5px", fontWeight: "bold" }} alt="Services Layer" />
                        <ListItemText
                            sx={{
                                "&:active": {
                                    color: "#e40046"
                                }
                            }}
                            primary="Services Layer"
                        />
                    </ListItemButton>
                </ListItem>
                <ListItem disablePadding selected={selected === "dbLayer"} className={classes.listItem}>
                    <ListItemButton component={Link} to={RoutesEnum.selectAppType} onClick={() => handleLocation("dbLayer")}>
                        <img src={database} style={{ width: "19px", marginRight: "5px", fontWeight: "bold" }} alt="Database Layer" />
                        <ListItemText
                            sx={{
                                "&:active": {
                                    color: "#e40046"
                                }
                            }}
                            primary="Database Layer"
                        />
                    </ListItemButton>
                </ListItem>
                <ListItem disablePadding selected={path === RoutesEnum.home} className={classes.listItem}>
                    <ListItemButton component={Link} to={RoutesEnum.home}>
                        <img src={scheduled} style={{ width: "19px", marginRight: "5px", fontWeight: "bold" }} alt="Scheduled Jobs" />
                        <ListItemText
                            sx={{
                                "&:active": {
                                    color: "#e40046"
                                }
                            }}
                            primary="Scheduled Jobs"
                        />
                    </ListItemButton>
                </ListItem>
            </List>
        </Box>
    );

    return (
        <div>
            {(["left"] as const).map((anchor) => (
                <Box key={anchor}>
                    <Drawer
                        anchor={anchor}
                        open={state[anchor]}
                        onClose={toggleDrawer(anchor, false)}
                        sx={{
                            "& .MuiDrawer-modal": {
                                backgroundColor: "#fff"
                            },
                            "& .MuiBackdrop-root": {
                                backgroundColor: "rgba(0, 0, 0, 0)"
                            }
                        }}
                    >
                        {list(anchor)}
                    </Drawer>
                </Box>
            ))}
        </div>
    );
};

export default TemporaryDrawer;
