import React, { useState } from "react";
import { useFormik } from "formik";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

import { Box, Button, Typography } from "@mui/material";

import { en } from "../../dictionary/en";
import RoutesEnum from "../../enums/Routes.enum";
import HoC from "../../Hoc";
import GenerateForm from "../../Hoc/GenerateForms";
import { PageData } from "../../inputconfig";
import { useAppSelector } from "../../Redux/Hooks";
import { handlePages } from "../../Redux/Reducers";
import { validateFormOnSubmit } from "../../ReusableCodes/Resuseablefunctions";
import { buttonStyle } from "../../Theme/Theme";

const GeneratePage = () => {
    const { pageData } = useAppSelector((state: any) => state);
    console.log("pageData: ", pageData);
    const dispatch = useDispatch();
    const [inputValue, setInputValue] = useState<any>({});
    const [Value, setValue] = useState<any>();
    const navigate = useNavigate();
    const handleclick = () => {
        if (Number(Value?.formId) === 1) {
            navigate(RoutesEnum.pageForm);
        } else if (Number(Value?.formId) === 2) {
            navigate(RoutesEnum.listcapture);
        } else if (Number(Value?.formId) === 3) {
            navigate(RoutesEnum.reportcapture);
        }
    };
    const formikProps = useFormik({
        initialValues: {
            ...PageData.map((e: any) => ({ [e.Name]: e.InitialValue })).reduce((a: any, b = {}) => ({ ...b, ...a })),
            pageName: pageData.pageName
        },
        onSubmit: async (values) => {
            setInputValue({ ...inputValue, ...values });
            dispatch(handlePages({ ...inputValue, ...values }));
        },
        validate: (values) => validateFormOnSubmit(values, [PageData])
    });
    return (
        <Box component="form" onClick={formikProps.handleSubmit}>
            <HoC.CardComponent>
                <Box component="div" width="100%" p={5}>
                    <Box width="100%" display="flex" justifyContent="space-between" paddingY={5}>
                        <Button
                            style={buttonStyle}
                            onClick={() => {
                                setInputValue({
                                    pageType: "DataForm"
                                });
                                setValue({ formId: 1 });
                            }}
                            className="NextButton"
                            sx={{ px: 2 }}
                        >
                            {en.dataform}
                        </Button>
                        <Button
                            style={buttonStyle}
                            onClick={() => {
                                setInputValue({
                                    pageType: "ListForm"
                                });
                                setValue({ formId: 2 });
                            }}
                            className="NextButton"
                        >
                            {en.listform}
                        </Button>
                        <Button
                            style={buttonStyle}
                            onClick={() => {
                                setInputValue({
                                    pageType: "ReportForm"
                                });
                                setValue({ formId: 3 });
                            }}
                            className="NextButton"
                        >
                            {en.reportform}
                        </Button>
                    </Box>
                    {(inputValue?.pageType || pageData?.pageType) && (
                        <Box>
                            <Typography variant="h6">
                                {en.pagetype} : {inputValue?.pageType || pageData?.pageType}
                            </Typography>
                        </Box>
                    )}

                    <GenerateForm FormData={PageData} FormikProps={formikProps} />
                    <Box p={2}>
                        <HoC.Navigators handleBack={() => navigate(RoutesEnum.managePage)} handleNext={handleclick} />
                    </Box>
                </Box>
            </HoC.CardComponent>
        </Box>
    );
};
export default GeneratePage;
