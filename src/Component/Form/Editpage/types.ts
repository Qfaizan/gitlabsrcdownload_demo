export type EditDataFormPageProps = {
    jsonDatavalue?: any;
    onPropsHandleChange: () => void;
};

export type EditListFormPageProps = {
    jsonDatavalue?: any;
    onPropsHandleChange: () => void;
};

export type EditReportFormPageProps = {
    jsonDatavalue?: any; // Replace with actual type
    onPropsHandleChange: () => void; // Replace with actual type
};
