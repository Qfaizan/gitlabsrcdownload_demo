import React, { useEffect, useState } from "react";
import { v4 as uuidv4 } from "uuid";

import { Add, Cancel, Delete, Edit, Save } from "@mui/icons-material";
import { Box, Button, Grid, Paper, Typography, useMediaQuery } from "@mui/material";
import { GridActionsCellItem, GridRowEditStopReasons, GridRowModes, GridToolbarContainer } from "@mui/x-data-grid";
import { GridRowModesModelProps } from "@mui/x-data-grid/models/api/gridEditingApi";

import API from "../../../API";
import { en, messages } from "../../../dictionary/en";
import HoC from "../../../Hoc";
import { downloadJsonFile, formatPageJson } from "../../../ReusableCodes/Resuseablefunctions";
import { buttonStyle, Spinner } from "../../../Theme/Theme";

import { EditDataFormPageProps } from "./types";

const AddRow: React.FC<{
    setRows: any;
    setRowModesModel: any;
}> = ({ setRows, setRowModesModel }) => {
    const handleClick = () => {
        const id = uuidv4();
        setRows((oldRows: any) => [
            ...oldRows,
            {
                id,
                fieldName: "",
                fieldType: "",
                fieldOrder: "",
                mandatory: "",
                isNew: true
            }
        ]);
        setRowModesModel((oldRowModel: any) => ({
            ...oldRowModel,
            [id]: {
                mode: GridRowModes.Edit,
                fieldToFocus: "fieldName"
            }
        }));
    };

    return (
        <GridToolbarContainer>
            <Button color="primary" startIcon={<Add />} onClick={handleClick}>
                {en.addfield}
            </Button>
        </GridToolbarContainer>
    );
};

const EditDataFormPage: React.FC<EditDataFormPageProps> = ({ jsonDatavalue, onPropsHandleChange }) => {
    console.log("jsonDatavalue", jsonDatavalue);

    const [loading, setLoading] = useState(false);
    const [jsonData, setJsonData] = useState<any>(jsonDatavalue);
    const [rows, setRows] = React.useState<any[]>([]);
    const [rowModesModel, setRowModesModel] = React.useState<{
        [key: string]: GridRowModesModelProps;
    }>({});

    const handleRowsEditStop = (params: any, event: any) => {
        if (params.reason === GridRowEditStopReasons.rowFocusOut) {
            event.defaultMuiPrevented = true;
        }
    };

    const handleEditClick = (id: any) => () => {
        setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
    };

    const hanldeDeleteClick = (id: string) => () => {
        setRows((row: any) => row.filter((e: any) => e.id !== id));
    };

    const handleCancelClick = (id: any) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: {
                mode: GridRowModes.View,
                ignoreModifications: true
            }
        });
        const editedRow = rows.find((row) => row.id === id);
        if (editedRow?.isNew) {
            setRows((r: any) => r.filter((row: any) => row.id !== id));
        }
    };

    const handleSaveClick = (id: any) => () => {
        setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.View } });
    };
    console.log("gk fix");
    const handleRowModesModelChange = (newRowModel: any) => {
        setRowModesModel(newRowModel);
    };

    const processRowUpdate = (newRow: any) => {
        setRows((a: any) => a.map((row: any) => (row.id === newRow.id ? newRow : row)));
        return newRow;
    };

    const isSmallScreen = useMediaQuery("(max-width:600px)");

    const columns = [
        {
            field: en.fieldname,
            headerName: en.fieldheader,
            editable: true,
            preProcessEditCellProps: (params: any) => {
                const hasError = params.props.value?.length < 1;
                return { ...params.props, error: hasError };
            }
        },
        {
            field: en.fieldtype,
            headerName: en.typeheader,
            type: en.typeselect,
            preProcessEditCellProps: (params: any) => {
                const hasError = !params.props.value;
                return { ...params.props, error: hasError };
            },
            editable: true,
            valueOptions: [
                { label: "Text", value: "Text" },
                { label: "Radio", value: "Radio" },
                { label: "Checkbox", value: "Checkbox" },
                { label: "Toggle", value: "Toggle" },
                { label: "Select", value: "Select" },
                { label: "Date", value: "Date" }
            ]
        },
        {
            field: en.fieldorder,
            headerName: en.orderheader,
            type: en.typenumber,
            preProcessEditCellProps: (params: any) => {
                const hasError = !params.props.value;
                return { ...params.props, error: hasError };
            },
            editable: true
        },
        {
            field: en.fieldmandatory,
            headerName: en.madatoryheader,
            valueOptions: [
                { label: "Yes", value: true },
                { label: "No", value: false }
            ],
            type: en.typeselect,
            editable: true
        },
        {
            field: en.fieldaction,
            type: en.fieldaction,
            cellClassName: en.fieldaction,
            getActions: ({ id }: { id: any }) => {
                const isInEditMode = rowModesModel[id] === (GridRowModes as any).Edit;

                if (isInEditMode) {
                    return [
                        <GridActionsCellItem icon={<Save />} label="Save" onClick={handleSaveClick(id)} />,
                        <GridActionsCellItem icon={<Cancel />} label="Cancel" onClick={handleCancelClick(id)} />
                    ];
                }
                return [
                    <GridActionsCellItem icon={<Edit />} label="Edit" onClick={handleEditClick(id)} />,
                    <GridActionsCellItem icon={<Delete />} label="Delete" onClick={hanldeDeleteClick(id)} />
                ];
            }
        }
    ];
    console.log(rows);
    useEffect(() => {
        setLoading(true);
        setJsonData(jsonDatavalue);
        if (jsonData?.pageDetails?.length) {
            setRows(
                jsonData?.pageDetails.map((field: any) => ({
                    fieldName: field?.fieldName,
                    fieldType: field?.fieldType?.label,
                    fieldOrder: field?.fieldOrder?.label,
                    mandatory: field?.mandatory?.value,
                    id: uuidv4()
                }))
            );
            setLoading(false);
        }
        setLoading(false);
    }, [jsonData]);

    useEffect(() => {
        setLoading(true);
        if (rows.length) {
            rows.forEach((row) => {
                setRowModesModel({
                    ...rowModesModel,
                    [row.id]: {
                        mode: GridRowModes.View
                    }
                });
            });
            setLoading(false);
        }
        setLoading(false);
    }, [rows]);

    const handleUpdatedJson = async () => {
        try {
            setLoading(true);
            const formattedJson = formatPageJson(rows, jsonData);
            downloadJsonFile({ ...formattedJson }, `${formattedJson?.pageName}.json`);
            const response: any = await API.Projects.updatePages(formattedJson);
            if (response?.success) {
                onPropsHandleChange();
                setLoading(false);
            }
        } catch (error) {
            console.error(messages.anErrormessage, error);
            setLoading(false);
        }
    };

    return (
        <Box component="div">
            {loading ? (
                <Spinner loading={loading} />
            ) : (
                <Box p={isSmallScreen ? 2 : 4} display="flex" justifyContent="center" alignItems="center" minHeight="100vh">
                    <Paper sx={{ padding: isSmallScreen ? 1 : 2 }} elevation={6}>
                        <Grid container minHeight="90vh" direction="column" justifyContent="space-around">
                            <Grid item xs={12}>
                                <Typography variant="h4">Edit {jsonData?.pageName} JSON</Typography>
                                <HoC.DataTable
                                    rows={rows || []}
                                    columns={columns}
                                    rowModesModel={rowModesModel}
                                    onRowEditStop={handleRowsEditStop}
                                    onRowModesModelChange={handleRowModesModelChange}
                                    processRowUpdate={processRowUpdate}
                                    slots={{
                                        footer: AddRow
                                    }}
                                    slotProps={{
                                        footer: {}
                                    }}
                                />
                            </Grid>

                            <Grid item display="flex" gap={2} justifyContent="space-between">
                                <Button
                                    variant="outlined"
                                    onClick={() => {
                                        onPropsHandleChange();
                                    }}
                                >
                                    {en.backbtn}
                                </Button>
                                <Button
                                    style={buttonStyle}
                                    onClick={handleUpdatedJson}
                                    sx={{
                                        paddingX: 2,
                                        paddingY: 1,
                                        color: "#000"
                                    }}
                                >
                                    <Typography>{en.downloadupjson}</Typography>
                                    {/* <ArrowForwardIosIcon />
               <ArrowForwardIosIcon /> */}
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                </Box>
            )}
        </Box>
    );
};

export default EditDataFormPage;
