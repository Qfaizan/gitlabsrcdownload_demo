import React, { useEffect, useState } from "react";
import { v4 as uuidv4 } from "uuid";

import { Add, Cancel, Delete, Edit, Save } from "@mui/icons-material";
import { Box, Button, Grid, Paper, Typography } from "@mui/material";
import { GridActionsCellItem, GridRowEditStopReasons, GridRowModes, GridToolbarContainer } from "@mui/x-data-grid";

import API from "../../../API";
import { en } from "../../../dictionary/en";
import HoC from "../../../Hoc";
import { downloadJsonFile, formatPageJson } from "../../../ReusableCodes/Resuseablefunctions";
import { buttonStyle, Spinner } from "../../../Theme/Theme";

import { EditReportFormPageProps } from "./types";

type Row = {
    id: string;
    columnName: string;
    columnValue: string;
    columnType: string;
    columnOrder: string;
    isNew?: boolean;
};

type RowModesModel = {
    [key: string]: {
        mode: GridRowModes;
        ignoreModifications?: boolean;
        fieldToFocus?: string;
    };
};

const AddRow: React.FC<{
    setRows: React.Dispatch<React.SetStateAction<Row[]>>;
    setRowModesModel: React.Dispatch<React.SetStateAction<RowModesModel>>;
}> = ({ setRows, setRowModesModel }) => {
    const handleClick = () => {
        const id = uuidv4();
        setRows((oldRows) => [
            ...oldRows,
            {
                id,
                columnName: "",
                columnValue: "",
                columnType: "",
                columnOrder: ""
            }
        ]);
        setRowModesModel((oldRowModel) => ({
            ...oldRowModel,
            [id]: {
                mode: GridRowModes.Edit,
                fieldToFocus: en.fieldname
            }
        }));
    };

    return (
        <GridToolbarContainer>
            <Button color="primary" startIcon={<Add />} onClick={handleClick}>
                {en.addfield}
            </Button>
        </GridToolbarContainer>
    );
};

const EditReportFormPage: React.FC<EditReportFormPageProps> = ({ jsonDatavalue, onPropsHandleChange }) => {
    const [loading, setLoading] = useState(false);
    const [jsonData, setJsonData] = useState(jsonDatavalue);
    const [rows, setRows] = useState<Row[]>([]);
    const [rowModesModel, setRowModesModel] = useState<RowModesModel>({});

    const handleRowsEditStop = (params: any, event: any) => {
        if (params.reason === GridRowEditStopReasons.rowFocusOut) {
            event.defaultMuiPrevented = true;
        }
    };

    const handleEditClick = (id: string) => () => {
        setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
    };

    const hanldeDeleteClick = (id: string) => () => {
        setRows(rows.filter((row) => row.id !== id));
    };

    const handleCancelClick = (id: string) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: {
                mode: GridRowModes.View,
                ignoreModifications: true
            }
        });
        const editedRow = rows.find((row) => row.id === id);
        if (editedRow?.isNew) {
            setRows(rows.filter((row) => row.id !== id));
        }
    };

    const handleSaveClick = (id: string) => () => {
        setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.View } });
        const editedRow = rows.find((row) => row.id === id);
        if (editedRow?.isNew) {
            // Handle save logic for new rows
        }
    };

    const handleRowModesModelChange = (newRowModel: RowModesModel) => {
        setRowModesModel(newRowModel);
    };

    const processRowUpdate = (newRow: Row) => {
        setRows((prevRows) => prevRows.map((row) => (row.id === newRow.id ? newRow : row)));
        return newRow;
    };

    const columns = [
        {
            field: en.reportclm,
            headerName: en.reportclmheader,
            editable: true,
            preProcessEditCellProps: (params: any) => {
                const hasError = (params.props.value || "").length < 1;
                return { ...params.props, error: hasError };
            }
        },
        {
            field: en.reportvalue,
            headerName: en.reporthead,
            editable: true,
            preProcessEditCellProps: (params: any) => {
                const hasError = (params.props.value || "").length < 1;
                return { ...params.props, error: hasError };
            }
        },
        {
            field: en.reporttype,
            headerName: en.reportheadtype,
            type: en.typeselect,
            preProcessEditCellProps: (params: any) => {
                const hasError = !params.props.value;
                return { ...params.props, error: hasError };
            },
            editable: true,
            valueOptions: [
                { label: "Text", value: "Text" },
                { label: "Radio", value: "Radio" },
                { label: "Checkbox", value: "Checkbox" },
                { label: "Toggle", value: "Toggle" },
                { label: "Select", value: "Select" },
                { label: "Date", value: "Date" }
            ]
        },
        {
            field: en.reportorder,
            headerName: en.reportheadorder,
            type: en.typenumber,
            preProcessEditCellProps: (params: any) => {
                const hasError = !params.props.value;
                return { ...params.props, error: hasError };
            },
            editable: true
        },
        {
            field: en.fieldaction,
            type: en.fieldaction,
            cellClassName: en.fieldaction,
            getActions: ({ id }: { id: string }) => {
                const isInEditMode = rowModesModel[id]?.mode === GridRowModes.Edit;
                if (isInEditMode) {
                    return [
                        <GridActionsCellItem icon={<Save />} label="Save" onClick={handleSaveClick(id)} />,
                        <GridActionsCellItem icon={<Cancel />} label="Cancel" onClick={handleCancelClick(id)} />
                    ];
                }
                return [
                    <GridActionsCellItem icon={<Edit />} label="Edit" onClick={handleEditClick(id)} />,
                    <GridActionsCellItem icon={<Delete />} label="Delete" onClick={hanldeDeleteClick(id)} />
                ];
            }
        }
    ];

    useEffect(() => {
        setJsonData(jsonDatavalue);
        if (jsonData?.pageDetails?.length) {
            setRows(
                jsonData?.pageDetails.map((field: any) => ({
                    columnName: field.columnName,
                    columnValue: field.columnValue,
                    columnType: field.columnType?.label,
                    columnOrder: field.columnOrder?.label,
                    id: uuidv4()
                }))
            );
        }
    }, [jsonDatavalue]);

    useEffect(() => {
        if (rows.length) {
            rows.forEach((row) => {
                setRowModesModel({
                    ...rowModesModel,
                    [row.id]: {
                        mode: GridRowModes.View
                    }
                });
            });
        }
    }, [rows]);

    useEffect(() => {
        console.log({ rowModesModel });
    }, [rowModesModel]);

    const handleUpdatedJson = async () => {
        setLoading(true);
        try {
            const formattedJson = formatPageJson(rows, jsonData);
            downloadJsonFile({ ...formattedJson }, `${formattedJson?.pageName}.json`);

            const response: any = await API.Projects.updatePages(formattedJson);
            if (response?.success) {
                onPropsHandleChange();
                setLoading(false);
            }
        } catch (error) {
            setLoading(false);
        }
    };

    return (
        <Box component="div">
            {loading ? (
                <Spinner loading={loading} />
            ) : (
                <Box p={4} display="flex" justifyContent="center" alignItems="center" minHeight="100vh">
                    <Paper sx={{ padding: "32px" }} elevation={6}>
                        <Grid container minHeight="90vh" direction="column" justifyContent="space-around">
                            <Grid item xs={12}>
                                <Typography variant="h4">Edit {jsonData?.pageName} JSON</Typography>
                                <HoC.DataTable
                                    rows={rows}
                                    columns={columns}
                                    editMode="row"
                                    rowModesModel={rowModesModel}
                                    onRowEditStop={handleRowsEditStop}
                                    onRowModesModelChange={handleRowModesModelChange}
                                    processRowUpdate={processRowUpdate}
                                    slots={{
                                        footer: AddRow
                                    }}
                                    slotProps={{
                                        footer: {}
                                    }}
                                />
                            </Grid>

                            <Grid item display="flex" gap={2} justifyContent="space-between">
                                <Button
                                    variant="outlined"
                                    onClick={() => {
                                        onPropsHandleChange();
                                    }}
                                >
                                    {en.backbtn}
                                </Button>
                                <Button
                                    style={buttonStyle}
                                    onClick={handleUpdatedJson}
                                    sx={{
                                        paddingX: 2,
                                        paddingY: 1,
                                        color: "#000"
                                    }}
                                >
                                    <Typography>{en.downloadupjson}</Typography>
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                </Box>
            )}
        </Box>
    );
};

export default EditReportFormPage;
