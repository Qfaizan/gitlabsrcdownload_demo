interface FormValues {
    fieldName?: string;
    fieldType?: { label: string };
    mandatory?: { label: string };
    fieldOrder?: { label: string };
    columnName?: string;
    columnType?: { label: string };
    columnOrder?: { label: string };
    ColumnName?: string;
    ColumnType?: { label: string };
    ColumnOrder?: { label: string };
}

export const detectFormFieldChanges = (values: FormValues, initialValues: FormValues[]) => {
    if (values.fieldName) {
        if (
            values.fieldName !== initialValues[0]?.fieldName &&
            values.fieldType?.label !== initialValues[0]?.fieldType?.label &&
            values.mandatory?.label !== initialValues[0]?.mandatory?.label &&
            values.fieldOrder?.label !== initialValues[0]?.fieldOrder?.label
        ) {
            return false;
        }
        return true;
    }
    if (values.columnName) {
        if (
            values.columnName !== initialValues[0]?.columnName &&
            values.columnType?.label !== initialValues[0]?.columnType?.label &&
            values.columnOrder?.label !== initialValues[0]?.columnOrder?.label
        ) {
            return false;
        }
        return true;
    }
    if (values.ColumnName) {
        if (
            values.ColumnName !== initialValues[0]?.ColumnName &&
            values.ColumnType?.label !== initialValues[0]?.ColumnType?.label &&
            values.ColumnOrder?.label !== initialValues[0]?.ColumnOrder?.label
        ) {
            return false;
        }
        return true;
    }
};
