/* eslint-disable no-underscore-dangle */
import React, { useState } from "react";
import { useFormik } from "formik";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

import { Box, Button, FormControlLabel, Grid, Paper, Switch, TextField, Tooltip, Typography } from "@mui/material";
import { GridRowEditStopReasons } from "@mui/x-data-grid";

import API from "../../../API";
import { en, messages } from "../../../dictionary/en";
import RoutesEnum from "../../../enums/Routes.enum";
import HoC from "../../../Hoc";
import CheckBox from "../../../Hoc/Checkbox";
import GenerateForm from "../../../Hoc/GenerateForms";
import { columns, contactDetails } from "../../../inputconfig";
import { useAppDispatch, useAppSelector } from "../../../Redux/Hooks";
import { handleDrawerClose, handleDrawerOpen, handlePageData, handlePages } from "../../../Redux/Reducers";
import { downloadJsonFile } from "../../../ReusableCodes/Resuseablefunctions";
import { buttonStyle } from "../../../Theme/Theme";
import CloningSourceCode from "../../codeCloning/CodeCloning";

import ProjectList from "./ProjectList/index";

interface DataCaptureFormProps {}

const DataCaptureForm: React.FC<DataCaptureFormProps> = () => {
    const { projectData, newPages, pageData } = useAppSelector((state) => state);
    console.log("newPages: ", newPages);
    const dispatch = useAppDispatch();
    const [isApi, setIsApi] = useState({
        isEnabled: false,
        apiUrl: "",
        inputParams: ""
    });
    const [state, setState] = useState<any[]>([]);
    const [defaultCheckbox, setDefaultCheckbox] = useState<boolean>(false);
    const [projectJson, setProjectJson] = useState<any>();
    const [selectedProject, setSelectedProject] = useState<any>();
    const [selectionModel, setSelectionModel] = useState<any[]>([]);
    const navigate = useNavigate();
    const handleOpen = () => {
        dispatch(handleDrawerOpen({ open: true, type: "IS_API" }));
    };

    const handleClose = () => {
        if (isApi.apiUrl === "" || isApi.inputParams === "") {
            setIsApi((prev) => ({ ...prev, isEnabled: false }));
        }
        dispatch(handleDrawerClose());
    };

    const handleDrawerSubmit = () => {
        console.log("1");
        const URLPATTERN = /^(https?:\/\/)?([\w-]+\.)+[\w-]+(\/[\w- ./?%&=]*)?$/;
        if (URLPATTERN.test(isApi.apiUrl)) {
            console.log("2");
            dispatch(handleDrawerClose());
        } else {
            toast.error("Invaild URL");
        }
    };

    const handleSwitch = (value: any) => {
        if (value.target.checked) handleOpen();
        setIsApi((prev) => ({ ...prev, isEnabled: value.target.checked }));
    };

    const handleDrawerBack = () => {
        setIsApi({ isEnabled: false, apiUrl: "", inputParams: "" });
        dispatch(handleDrawerClose());
    };
    const handleIsApi = (event: any) => {
        setIsApi((prev: any) => ({
            ...prev,
            [event.target.name]: event.target.value
        }));
    };
    const handleRowEditStop = (params: any, event: any) => {
        if (params.reason === GridRowEditStopReasons.rowFocusOut) {
            event.defaultMuiPrevented = true;
        }
    };

    const processRowUpdate = (newRow: any) => {
        const updatedRow = { ...newRow, isNew: false };
        setState((prevState: any) => prevState.map((r: any) => (r.id === newRow.id ? updatedRow : r)));

        return updatedRow;
    };
    const handleDeleteRows = async () => {
        try {
            const updatedState = state.filter((item: any) => !selectionModel.includes(item.id));
            await dispatch(handlePages({ ...newPages, updatedState }));
            setState(updatedState);
        } catch (error) {
            console.error(messages?.anErrormessage, error);
        }
    };
    const formikProps = useFormik({
        initialValues: {
            ...contactDetails.map((e: any) => ({ [e.Name]: e.InitialValue })).reduce((a: any, b = {}) => ({ ...b, ...a }))
        },
        onSubmit: async (values) => {
            if (isApi.isEnabled === true) {
                setState([...state, { ...values, isApi: { ...isApi } }]);
                dispatch(
                    handlePages({
                        isApi: { ...isApi },
                        pageDetails: [...newPages.pageDetails, values]
                    })
                );
            } else {
                setState([...state, values]);
                dispatch(handlePages({ pageDetails: [...newPages.pageDetails, values] }));
            }
        }
    });
    const handleCheck = async (defaultCheck: boolean) => {
        if (!state.length) {
            toast.warning(messages.kindlyProvideDetails);
        } else {
            dispatch(handleDrawerOpen({ open: true, type: en.appendProjectType }));
            setDefaultCheckbox(defaultCheck);
        }
    };
    const handleProjectModal = async (project: any) => {
        setSelectedProject(project);
        const file: any = project;
        if (file) {
            try {
                const updatedProject = {
                    ...file,
                    // eslint-disable-next-line no-unsafe-optional-chaining
                    pages: [...file?.pages, newPages],
                    IsAddInProject: true
                };
                setProjectJson(updatedProject);
                await API.Projects.updateProjects(updatedProject);
            } catch (parseError) {
                console.error(messages.jsonparseerror, parseError);
            }
        }
    };
    const handleAppendProjectCancel = () => {
        if (!selectedProject) {
            setDefaultCheckbox(false);
        }
        dispatch(handleDrawerClose());
    };
    const handleApppendProjectOk = () => {
        if (selectedProject) {
            dispatch(handleDrawerClose());
            setDefaultCheckbox(true);
        } else {
            toast.error(messages?.PleaseChooseProject);
        }
    };
    const handleDownloadJson = async () => {
        const response: any = await API.Projects.createPages(newPages);
        if (response?.success) {
            downloadJsonFile({ ...newPages }, `${newPages?.pageName}.json`);
            dispatch(handlePageData([...pageData, response?.data]));
            navigate(RoutesEnum.managePage);
        }
    };
    return (
        <>
            <Box component="form" onSubmit={formikProps.handleSubmit}>
                <Box minHeight="90vh" display="flex" justifyContent="center" alignItems="center" p={4}>
                    <Paper sx={{ padding: "32px" }} elevation={6}>
                        <Grid container spacing={2}>
                            {/* Field Grid */}
                            <Grid item xs={12}>
                                <Box width="100%" display="flex" justifyContent="center" flexDirection="column" alignItems="flex-start">
                                    <Typography title="Personal Details" variant="h5" fontWeight={700} marginBottom={1} mt={2}>
                                        {en.dataform}
                                    </Typography>
                                    <Typography variant="h6">{newPages?.initialState?.pageName || newPages?.pageName}</Typography>
                                </Box>
                            </Grid>
                            <Grid item sm={6} p={2}>
                                <GenerateForm FormData={contactDetails} FormikProps={formikProps} />
                                {formikProps?.values?.fieldType?.value === "Radio" ||
                                formikProps?.values?.fieldType?.value === "Checkbox" ||
                                formikProps?.values?.fieldType?.value === "Select" ? (
                                    <HoC.Input
                                        value={formikProps?.values?.fieldType?.options}
                                        label="Options"
                                        name="options"
                                        type="text"
                                        required
                                        toolTip="Enter Opions separated by comma"
                                        onChange={(value: any) => formikProps.setFieldValue("fieldType.options", value)}
                                    />
                                ) : null}
                                {/* Is API field  */}
                                <FormControlLabel control={<Switch checked={isApi.isEnabled} onChange={handleSwitch} />} label="Is API" />
                                <Grid display="flex" justifyContent="space-between" py={1} gap={2} mt={1}>
                                    {selectionModel.length > 0 ? (
                                        <Button
                                            variant="contained"
                                            color="inherit"
                                            disabled={!(selectionModel.length > 0)}
                                            onClick={handleDeleteRows}
                                        >
                                            {en.deletebtn}
                                        </Button>
                                    ) : (
                                        <Button variant="contained" color="inherit" onClick={() => formikProps.resetForm()}>
                                            {en.cancelbtn}
                                        </Button>
                                    )}
                                    <Button style={buttonStyle} type="submit">
                                        Add
                                    </Button>
                                </Grid>
                            </Grid>
                            {/* Table Grid */}
                            <Grid item xs={12} sm={6} p={2}>
                                <HoC.DataTable
                                    rows={state.map((row, index) => ({
                                        ...row,
                                        id: index + 1,
                                        fieldType: row?.fieldType?.label || row?.fieldType,
                                        mandatory: row?.mandatory?.label || row?.mandatory,
                                        fieldOrder: row?.fieldOrder?.label || row?.fieldOrder,
                                        options: row?.fieldType?.options || row?.fieldType?.options
                                    }))}
                                    columns={columns}
                                    initialState={{
                                        pagination: {
                                            paginationModel: { page: 0, pageSize: 5 }
                                        }
                                    }}
                                    pageSizeOptions={[5, 10]}
                                    checkboxSelection
                                    onRowSelectionModelChange={(newRowSelectionModel: React.SetStateAction<any[]>) => {
                                        setSelectionModel(newRowSelectionModel);
                                        setState((prevState) =>
                                            prevState.map((row, index) => ({
                                                ...row,
                                                id: index + 1,
                                                fieldType: row?.fieldType,
                                                mandatory: row?.mandatory,
                                                fieldOrder: row?.fieldOrder
                                            }))
                                        );
                                    }}
                                    onRowEditStop={handleRowEditStop}
                                    processRowUpdate={processRowUpdate}
                                />
                            </Grid>
                            <CheckBox
                                id="appendWithProject"
                                name="appendWithProject"
                                checked={defaultCheckbox}
                                onChange={handleCheck}
                                disabled={false}
                                label="Select to Append with Project"
                                toolTip="Select to Append with Project"
                            />
                            <Grid item py={1} display="flex" justifyContent="space-between" gap={2} lg={12} xs={12} sm={12}>
                                <Button
                                    variant="outlined"
                                    onClick={() => {
                                        navigate(RoutesEnum.pageType);
                                    }}
                                >
                                    {en.backbtn}
                                </Button>

                                {defaultCheckbox && projectJson ? (
                                    <CloningSourceCode jsonData={projectJson} />
                                ) : (
                                    <Button style={buttonStyle} disabled={!(state?.length > 0)} onClick={handleDownloadJson}>
                                        {en.genjson}
                                    </Button>
                                )}
                            </Grid>
                        </Grid>
                    </Paper>
                </Box>
                {/* IS API Drawer */}
                <HoC.CustomDrawer type="IS_API" handleDrawerOnClose={handleClose} title="API Config">
                    <Grid container px={2} py={4} rowGap={2}>
                        <Grid item xs={12} pt={2}>
                            <Box
                                paddingY={1}
                                width="100%"
                                display="flex"
                                justifyContent="space-around"
                                gap={2}
                                alignItems="center"
                                position="relative"
                            >
                                <Tooltip title="Enter API URL" placement="top-start">
                                    <TextField
                                        fullWidth
                                        name="apiUrl"
                                        defaultValue={isApi.apiUrl}
                                        type="text"
                                        onChange={handleIsApi}
                                        placeholder="Enter API url"
                                        required
                                        label="API URL"
                                        // error={error}
                                        value={isApi.apiUrl}
                                    />
                                </Tooltip>
                            </Box>
                        </Grid>
                        <Grid item xs={12} pt={2}>
                            <Box
                                paddingY={1}
                                width="100%"
                                display="flex"
                                justifyContent="space-around"
                                gap={2}
                                alignItems="center"
                                position="relative"
                            >
                                <Tooltip title="Enter API URL" placement="top-start">
                                    <TextField
                                        fullWidth
                                        name="inputParams"
                                        defaultValue={isApi.inputParams}
                                        type="text"
                                        onChange={handleIsApi}
                                        placeholder="Enter Input Params"
                                        required
                                        label="Input Params "
                                        // error={error}
                                        value={isApi.inputParams}
                                    />
                                </Tooltip>
                            </Box>
                        </Grid>
                        <Grid item xs={12} pt={2}>
                            <Box
                                sx={{
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "space-between"
                                }}
                            >
                                <Button onClick={handleDrawerBack}>
                                    <Typography>Back</Typography>
                                </Button>
                                <Button style={buttonStyle} onClick={handleDrawerSubmit}>
                                    <Typography>Submit</Typography>
                                </Button>
                            </Box>
                        </Grid>
                    </Grid>
                </HoC.CustomDrawer>
            </Box>
            {/* Append Project Drawer */}
            <HoC.CustomDrawer type="APPEND_PROJECT" handleDrawerOnClose={handleClose} title="Project List">
                <ProjectList
                    projectData={projectData}
                    selectedProject={selectedProject}
                    handleProjectModal={handleProjectModal}
                    handleCancel={handleAppendProjectCancel}
                    handleOk={handleApppendProjectOk}
                />
            </HoC.CustomDrawer>
        </>
    );
};

export default DataCaptureForm;
