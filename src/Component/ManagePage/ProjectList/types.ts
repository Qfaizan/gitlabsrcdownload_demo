export interface projectListProps {
    projectData: [any];
    selectedProject: any;
    handleProjectModal: (value: any) => void;
    handleCancel: () => void;
    handleOk: () => void;
}
