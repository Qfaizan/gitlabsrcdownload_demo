import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Button } from "@mui/material";

import { updateLoginType } from "../Reduxt/action";

const UpdateConfig: React.FC = () => {
    const config = useSelector((state: any) => state.config);
    const dispatch = useDispatch();

    const [newLoginType, setNewLoginType] = useState<string>("");

    const handleLoginTypeChange = () => {
        dispatch(updateLoginType(newLoginType));
    };

    return (
        <div>
            <p>Current Login Type: {config.loginType}</p>
            <input type="text" placeholder="New Login Type" value={newLoginType} onChange={(e) => setNewLoginType(e.target.value)} />
            <Button onClick={handleLoginTypeChange}>Change Login Type</Button>
        </div>
    );
};

export default UpdateConfig;
