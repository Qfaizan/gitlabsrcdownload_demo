const ConfigEnum: any = {
    uploadProjectJson: "PROJECTJSON",
    uploadPageJson: "PAGEJSON"
};

export default ConfigEnum;
